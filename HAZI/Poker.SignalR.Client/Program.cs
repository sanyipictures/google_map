﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.SignalR.Client
{
    class Player
    {
        public int Id { get; set; }
        public string name { get; set; }
        public int money { get; set; }
        public override string ToString()
        {
            return $"#{Id} {name} {money} @ :)";
        }
    }

    class Program
    {
        static async void RefreshList(IHubProxy signalr)
        {
            Console.WriteLine("FETCHING FROM SIGNALR");
            var list = await signalr.Invoke<IEnumerable<Player>>("GetAllUser");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("PRESS ENTER");
            Console.ReadKey();

            String url = "http://localhost:8585";
            var hubconnection = new HubConnection(url);
            var hubproxy = hubconnection.CreateHubProxy("PokerHub");

            hubproxy.On("Refresh", () =>
            {
                Console.WriteLine("REFRESH CALL FROM SERVER");
                RefreshList(hubproxy);
            });
            hubproxy.On<Player>("LoginUser", param => {
                Console.WriteLine("LOGGED IN PLAYER FROM SERVER:" + param);
            });
            hubconnection.Start().ContinueWith(task => {

                if (task.IsFaulted)
                {
                    Console.WriteLine("ERROR" + task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("CONNECTED");
                }
            }).Wait();
            Console.ReadKey();
            RefreshList(hubproxy);
            Console.ReadKey();
            Console.WriteLine("REGISTERING: Hungary");
            Player p = new Player() { Id = 1, money = 5000, name = "Hungary" };
            hubproxy.Invoke("RegisterUser", p).Wait();
            Console.WriteLine("REGISTERED");

            RefreshList(hubproxy);
            Console.ReadKey();

            Console.WriteLine("REGISTERING: Adam");
            p = new Player() { Id = 2, money = 5000, name = "Adam" };
            hubproxy.Invoke("RegisterUser", p).Wait();
            Console.WriteLine("REGISTERED");

            RefreshList(hubproxy);
            Console.ReadKey();

            Console.WriteLine("REGISTERING: Eva");
            p = new Player() { Id = 3, money = 5000, name = "Eva" };
            hubproxy.Invoke("RegisterUser", p).Wait();
            Console.WriteLine("REGISTERED");

            RefreshList(hubproxy);
            Console.ReadKey();

            Console.WriteLine("DELETING Hungary");
            hubproxy.Invoke("DeleteUserById", 1).Wait();
            Console.WriteLine("DELETED");

            Console.ReadKey();

            Console.WriteLine("DELETING Adam");
            hubproxy.Invoke("DeleteUserById", 2).Wait();
            Console.WriteLine("DELETED");

            Console.ReadKey();

            Console.WriteLine("DELETING Eva");
            hubproxy.Invoke("DeleteUserById", 3).Wait();
            Console.WriteLine("DELETED");

            Console.ReadKey();

            hubconnection.Stop();
            Console.WriteLine("STOPPED");
            Console.ReadKey();
        }
    }
}
