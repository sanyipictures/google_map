﻿using AutoMapper;
using Poker.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.SignalR.Server
{
    public class UserDTO
    {
        public static IMapper Mapper { get; private set; }
        static UserDTO()
        {
            Mapper = new MapperConfiguration(cfg => {
                cfg.CreateMap<UserDTO, UserLogicDTO>().ReverseMap();
            }).CreateMapper();
        }
        public int Id { get; set; }
        public string name { get; set; }
        public int money { get; set; }
    }
}
