﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.SignalR.Server
{
    public class Program
    {
        static void Main(string[] args)
        {
            String url = "http://localhost:8585";
            using (WebApp.Start(url))
            {
                System.Console.WriteLine("SERVER UP");
                System.Console.ReadKey();
            }
        }
    }
}
