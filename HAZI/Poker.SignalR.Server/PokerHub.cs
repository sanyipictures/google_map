﻿using Microsoft.AspNet.SignalR;
using Poker.Data;
using Poker.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.SignalR.Server
{
    public class PokerHub : Hub
    {
        IUser<UserLogicDTO> user;

        public PokerHub()
        {
            this.user = new LogicSum();
        }

        public void DeleteUserById(int id)
        {
            this.user.DeleteUserById(id);
            Clients.All.Refresh();
        }

        public UserDTO LoginUser(string usernameOrEmail, string password)
        {
            UserLogicDTO usr = this.user.LoginUser(usernameOrEmail, password);
            return UserDTO.Mapper.Map<UserLogicDTO, UserDTO>(usr);
        }

        public void RegisterUser(UserDTO user)
        {
            UserLogicDTO usr = UserDTO.Mapper.Map<UserDTO, UserLogicDTO>(user);
            this.user.RegisterUser(usr);
            Clients.All.NewUser(user);
        }

        public IEnumerable<UserDTO> GetAllUser()
        {
            List<UserLogicDTO> alluser = this.user.GetAllUsers();
            return UserDTO.Mapper.Map<List<UserLogicDTO>, IEnumerable<UserDTO>>(alluser);
        }
    }
}
