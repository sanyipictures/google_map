﻿using Poker.Repo.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Repo
{
    /// <summary>
    /// Represents a set of repository classes.
    /// </summary>
    public class Repository
    {
        public IUserBehavior UserRep { get; private set; }

        public Repository(IUserBehavior userRep)
        {
            this.UserRep = userRep;
        }
    }
}
