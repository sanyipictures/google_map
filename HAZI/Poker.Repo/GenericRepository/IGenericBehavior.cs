﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Repo.GenericRepository
{
    /// <summary>
    /// Descriebes the generic behavior of the database entitys.
    /// </summary>
    /// <typeparam name="TEntity">generic type of entities</typeparam>
    public interface IGenericBehaviour<TEntity> where TEntity : class
    {
        /// <summary>
        /// Inserts an entity.
        /// </summary>
        /// <param name="entity">an instance of an entity to be inserted</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Removes an entity from database by it's id.
        /// </summary>
        /// <param name="id">the id of the entitiy whith is marked to delete</param>
        void Delete(int id);

        /// <summary>
        /// Removes the given entity from the database
        /// </summary>
        /// <param name="entity">the instace to be removed</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Gives an entity instance by it's id.
        /// </summary>
        /// <param name="id">the id of the instance to be given</param>
        /// <returns>the instance, with the given id</returns>
        TEntity GetByID(int id);

        /// <summary>
        /// Gives an IQueryable list of entitys.
        /// </summary>
        /// <returns>all of the given type of entitys in database</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Gives a list of entities(can be single one as well), whitch are matched to the given condition.
        /// </summary>
        /// <param name="condition">lambda expression to filter the resoult set</param>
        /// <returns>a list of entitys matched by the condition</returns>
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition);
    }
}
