﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Repo.GenericRepository
{
    public abstract class GenericBehavior<TEntity> : IGenericBehaviour<TEntity> where TEntity : class
    {
        protected DbContext context;
        public GenericBehavior(DbContext ctx)
        {
            this.context = ctx;
            context.Database.Log = Console.WriteLine;
        }

        public void Delete(int id)
        {
            TEntity entity = GetByID(id);
            if (entity == null) throw new ArgumentException("NO DATA");
            Delete(entity);
        }

        public abstract TEntity GetByID(int id);

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return this.GetAll().Where(condition);
        }

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public void Insert(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            context.SaveChanges();
        }

        
    }
}
