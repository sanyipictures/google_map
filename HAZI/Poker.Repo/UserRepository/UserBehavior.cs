﻿using Poker.Data;
using Poker.Repo.GenericRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Repo.UserRepository
{
    public class UserBehavior : GenericBehavior<User>, IUserBehavior
    {
        public UserBehavior(DbContext ctx) : base(ctx)
        {
        }

        public override User GetByID(int id)
        {
            return Get(user => user.Id == id).SingleOrDefault();
        }

        public void Modify(int id, USERATTRIBUTE attributeType, string newAttribute)
        {
            User aktualUser = this.GetByID(id);
            if (aktualUser == null)
            {
                throw new ArgumentNullException("NO DATA");
            }
            if (newAttribute != null)
            {
                if (attributeType == USERATTRIBUTE.EMAIL)
                {
                    aktualUser.email = newAttribute;
                    this.context.SaveChanges();
                    return;
                }
                if (attributeType == USERATTRIBUTE.NAME)
                {
                    aktualUser.name = newAttribute;
                    this.context.SaveChanges();
                    return;
                }
                if (attributeType == USERATTRIBUTE.PASSWORD)
                {
                    aktualUser.password = newAttribute;
                    this.context.SaveChanges();
                }
            }
        }

    }
}
