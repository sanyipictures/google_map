﻿using Poker.Data;
using Poker.Repo.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Repo.UserRepository
{
    /// <summary>
    /// To simplify the modification.
    /// </summary>
    public enum USERATTRIBUTE { NAME, EMAIL, PASSWORD }

    /// <summary>
    /// Describes the specific behavior of a user.
    /// </summary>
    public interface IUserBehavior : IGenericBehaviour<User>
    {
        /// <summary>
        /// Modifies a user attribute.
        /// </summary>
        /// <param name="id">the id of the user to be modified</param>
        /// <param name="newAttribute">the attribute type to be modified</param>
        void Modify(int id, USERATTRIBUTE attributeType, string newAttribute);
    }
}
