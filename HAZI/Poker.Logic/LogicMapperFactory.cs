﻿using AutoMapper;
using Poker.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Logic
{
    /// <summary>
    /// Maps entitys to logic level classess, representing entities by matching names.
    /// </summary>
    public class LogicMapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<User, UserLogicDTO>().ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}
