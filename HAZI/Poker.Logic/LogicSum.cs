﻿using AutoMapper;
using Poker.Data;
using Poker.Repo.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Logic
{
    public class LogicSum : IUser<UserLogicDTO>
    {

        protected UserBehavior repo;
        IMapper mapper;

        public LogicSum()
        {
            PokaerDatabaseEntities ED = new PokaerDatabaseEntities();
            repo = new UserBehavior(ED);
            mapper = LogicMapperFactory.CreateMapper();
        }

        public void DeleteUserById(int id)
        {
            this.repo.Delete(id);
        }

        public List<UserLogicDTO> GetAllUsers()
        {
            IQueryable<User> users = this.repo.GetAll();
            return mapper.Map<IQueryable<User>, List<UserLogicDTO>>(users);
        }

        public UserLogicDTO LoginUser(string usernameOrEmail, string password)
        {
            User list = this.repo.Get(user => ((user.name == usernameOrEmail) || (user.email == usernameOrEmail)) && user.password == password).SingleOrDefault();
            return mapper.Map<User, UserLogicDTO>(list);
        }

        public void RegisterUser(UserLogicDTO user)
        {
            User usr = mapper.Map<UserLogicDTO, User>(user);
            this.repo.Insert(usr);
        }
    }
}
