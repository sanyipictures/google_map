﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Logic
{
    public interface IUser<TEntity> where TEntity : class
    {
        /// <summary>
        /// Registers a user.
        /// </summary>
        /// <param name="user">the user entity to be registered</param>
        void RegisterUser(TEntity user);

        /// <summary>
        /// Logs in the user.
        /// </summary>
        /// <param name="usernameOrEmail">the user's username, or password</param>
        /// <param name="password">the user's password</param>
        /// <returns>a user representing entity</returns>
        TEntity LoginUser(string usernameOrEmail, string password);

        List<TEntity> GetAllUsers();

        /// <summary>
        /// Removes the given user by it's id.
        /// </summary>
        /// <param name="id">the id of the player to be removed</param>
        void DeleteUserById(int id);
    }
}
