﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Hazi4
{
    class DownloadResources
    {
        static SemaphoreSlim sem = new SemaphoreSlim(3);
        private const int MAXATTEMP = 15;

        private Object toLock = new Object();

        public List<Xdoc> Xdocs { get; private set; }
        public String[] Famouses { get; private set; }

        public DownloadResources() {
            Xdocs = new List<Xdoc>();
        }

        public async Task DownloadFamous(String URI) {
            Console.WriteLine("elkezdődött a letöltés");
            String downloaded = await new HttpClient().GetStringAsync(URI);
            Console.WriteLine("elvileg vége a letöltésnek: ");
            Famouses = downloaded.Split(new[] { Environment.NewLine },
                                                        StringSplitOptions.None);
        }

        public async Task GetLocationXML(String APIURI) {
            sem.Wait();
            Console.WriteLine("semifogalás!");
            String downloaded = await new HttpClient().GetStringAsync(APIURI);
            Thread.Sleep(5000);
            Console.WriteLine("semi elengedés!");
            sem.Release();
            XDocument xd = XDocument.Parse(downloaded);

            if (xd.Descendants("status").First().Value.Equals("OK"))
            {
                ProcessXdocument(xd);
                Console.WriteLine("vége a feldolgozásnak");
            }
            if (xd.Descendants("status").First().Value.Equals("OVER_QUERY_LIMIT")) {
                int counter = 0;
                while ((xd.Descendants("status").First().Value.Equals("OVER_QUERY_LIMIT")) && counter < MAXATTEMP) {
                    Console.WriteLine("alszom!");
                    Thread.Sleep(5000);
                    Console.WriteLine("felkeltem!");
                    downloaded = await new HttpClient().GetStringAsync(APIURI + Famouses[0]);
                    xd = XDocument.Parse(downloaded);
                    ++counter;
                    Console.WriteLine("counter = {0}", counter);
                }
                if (counter == MAXATTEMP)
                {
                    Console.WriteLine("nem hajlandó normális választ adni");
                }
                else {
                    ProcessXdocument(xd);
                    Console.WriteLine("vége a feldolgozásnak");
                }
            }
        }
        public void ProcessXdocument(XDocument xd) {
            Xdoc q1 = xd.Descendants().Select(x => new Xdoc()
            {
                Status = x.Element("status")?.Value ?? "notFound",
                FormattedAddress = x.Descendants("formatted_address")?.First().Value ?? "notFound",
                Country = x.Descendants("address_component").ElementAt(5)?.Elements()?.First()?.Value ?? "notFound",
                Latitude = double.Parse(x.Descendants("lat").First()?.Value.Replace(".", ",")),
                Longitude = double.Parse(x.Descendants("lng").First()?.Value.Replace(".", ","))

            }).First();
            lock (this.toLock)
            {
                Console.WriteLine("hozzáadás lockolva");
                this.Xdocs.Add(q1);
            }
        }
    }
}
