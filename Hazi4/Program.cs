﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hazi4
{
    class Program
    {

        static void Main(string[] args)
        {
            double myLatitude = 47.4979120;
            double myLongitude = 19.0402350;
            String famousPlacesUri = "http://www.szabozs.hu/_DEBRECEN/kerteszg/famousplaces.txt";
            String APIURI = "https://maps.googleapis.com/maps/api/geocode/xml?address=";

            DownloadResources dr = new DownloadResources();

            List<Task> processingTasks = new List<Task>();
            List<double> distances = new List<double>();

            Task famousCollector = Task.Run(() => dr.DownloadFamous(famousPlacesUri));

            Task.WhenAny(famousCollector).ContinueWith(x =>
            {
                foreach (String uris in dr.Famouses)
                {
                    processingTasks.Add(Task.Run(() => dr.GetLocationXML(APIURI + uris)));
                }

                Task.WhenAll(processingTasks).ContinueWith(y =>
                {
                    foreach (var item in dr.Xdocs)
                    {
                        Console.WriteLine(item.ToString());
                        Console.WriteLine("*********************************");
                    }
                    Task.Run(() =>
                    {
                        var q1 = dr.Xdocs.Select(
                                z => new
                                {
                                    Dist = Distance.HaversineInM(myLatitude, myLongitude, z.Latitude, z.Longitude),
                                    Place = z.FormattedAddress
                                }
                            );
                        Console.WriteLine("Soroljuk fel az első 10, ehhez a geokoordinátához legközelebbi helyet (légvonalban) a gyűjteményből, távolság szerinti növekvő sorrendben!");
                        foreach (var item in q1.OrderByDescending(s => s.Dist).Take(10))
                        {
                            Console.WriteLine("Hely: {0}, Távolság: {1}meter", item.Place, item.Dist);
                        }
                        Console.WriteLine("Mely országban található a legtöbb nevezetesség? Melyek ezek?");
                        var q2 = dr.Xdocs.GroupBy(z => z.Country).Select(s => new
                        {
                            Name = s.Key,
                            Amount = s.Count()
                        }).OrderByDescending(h => h.Amount);
                        foreach (var item in q2)
                        {
                            Console.WriteLine("Ország: {0}, Mennyiség: {1}", item.Name, item.Amount);
                        }
                        Console.WriteLine("Milyen messze van a két legközelebbi nevezetesség egymástól?");
                        var q3 = dr.Xdocs.Select(z => new
                        {
                            CountyDistances = dr.Xdocs.Select(zh => new
                            {
                                Dist = Distance.HaversineInM(z.Latitude, z.Longitude, zh.Latitude, zh.Longitude),
                                Name2 = zh.FormattedAddress
                            }).OrderBy(doga => doga.Dist).First(),
                            Name = z.FormattedAddress
                        }).Distinct().OrderBy(kutya => kutya.CountyDistances.Dist).First();
                        Console.WriteLine("{0} {1}-től {2}km-re van", q3.Name, q3.CountyDistances.Name2, q3.CountyDistances.Dist);

                        Console.WriteLine("Melyik a két egymástól legnagyobb távolságban levő nevezetes hely, amelyek azonos országban vannak ? Mekkora ez a táv ? ");
                        var q4 = dr.Xdocs.Select(z => new
                        {
                            CountyDistances = dr.Xdocs.Select(zh => new
                            {
                                Dist = Distance.HaversineInM(z.Latitude, z.Longitude, zh.Latitude, zh.Longitude),
                                Name2 = zh.Country
                            }),
                            Name = z.Country
                        }).OrderBy(z => z.Name).GroupBy(z => z.Name).Select(z => new
                        {
                            Name = z.Key,
                            Amount = z.Select(macska => macska.CountyDistances.First().Dist).Max() - z.Select(macska => macska.CountyDistances.First().Dist).Min()
                        });
                        foreach (var item in q4)
                        {
                            Console.WriteLine("Ország: {0}-ban a két legtávolabbi nevezetesség távolsága: {1}", item.Name, item.Amount);
                        }
                    });
                });

                
            });
            Console.WriteLine("vége");
            Console.ReadKey();
        }
    }
}
