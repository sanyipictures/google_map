﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hazi4
{
    class Xdoc
    {
        public String Status { get; set; }
        public String FormattedAddress { get; set; }
        public String Country { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .Append("Status: ")
                .Append(Status)
                .Append("\nFormattedAddress: ")
                .Append(FormattedAddress)
                .Append("\nCountry: ")
                .Append(Country)
                .Append("\nLongitude: ")
                .Append(Longitude)
                .Append("\nLatitude: ")
                .Append(Latitude)
                .ToString();
        }
    }
}
